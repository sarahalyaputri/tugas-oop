<?php

require_once 'animal.php';
require_once 'frog.php';
require_once 'apes.php';

$sheep = new Animal("shaun");

echo "Nama : $sheep->name <br>"; // "shaun"
echo "Jumlah Kaki : $sheep->legs <br>"; // 2
echo " Berdarah dingin : $sheep->cold_blooded <br> <br>"; // false

$kodok = new Frog("buduk");
echo "Nama : $kodok->name <br>";
echo "Jumlah Kaki : $kodok->legs <br>"; // 2
echo " Berdarah dingin : $sheep->cold_blooded  <br>"; // false
$kodok->jump() ; // "hop hop"

$sungokong = new apes("kera sakti");
echo "Nama : $sungokong->name <br>";
echo "Jumlah Kaki : $sungokong->legs <br>"; // 2
echo " Berdarah dingin : $sungokong->cold_blooded  <br>";
$sungokong->yell(); // "Auooo"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

?>
